# bingo-subwoofer


### Building the website:
1. Download the Hugo docker image:
`docker pull klakegg/hugo:0.101.0-alpine`
2. Create a Hugo website skeleton:
  * MacOS / Linux:
    `docker run -it --rm -v ${PWD}:/src -p 1313:1313 klakegg/hugo:0.101.0-alpine new site mysite`
  * Windows:
    `docker run -it --rm -v .\:/src -p 1313:1313 klakegg/hugo:0.101.0-alpine new site mysite`
  - WARNING: DON'T BIND THE ROOT DIRECTORY TO THE CONTAINER
3. Download the Congo theme from [Hugo website](https://themes.gohugo.io/themes/congo/).
* Add the theme to `mysite/themes`
* Remove or rename `mysite/config.toml` (the default config file that Hugo searches for) 
* Copy all of the directories from the example site `mysite/themes/congo/exampleSite` to `mysite`

* Removing some clutter:

```
1. Rename `mysite/content/users` to `mysite/content/about`
2. Copy `mysite/content/about` and rename it to `mysite/content/publications`
3. Open `mysite/config/_default/menus.en.toml`
4. Remove the lines pertaining to docs and samples
5. Rename users to about
6. Add a new entry for publications
7. Try changing the weights
8. Restarting the server may be required
  Press ctrl + C and then re-run the most recent command
```

4. Make changes to your new Hugo website

  See the presentation for details 

5. Run a local Hugo server:
MacOS / Linux: `docker run --rm -it -v ${PWD}:/src -p 1313:1313 klakegg/hugo:0.101.0-alpine server --poll 700ms`
Windows: `docker run --rm -it -v .\:/src -p 1313:1313 klakegg/hugo:0.101.0-alpine server --poll 700ms`
6. Find your website on `//localhost:1313`

## Appendix

If you want to access the prompt within the Hugo container, run (does not work for the `latest` docker image):
    `docker run -it --rm -v ${PWD}:/src -p 1313:1313 klakegg/hugo:0.101.0-alpine shell`

Docker image: https://polybox.ethz.ch/index.php/s/LG0o7HnRVv62nD9


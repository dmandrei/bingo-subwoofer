---
title: "Publications"
date: 2020-08-14
draft: false
description: 
slug: "publications"
tags: ["users", "sample"]
showDate: false
showAuthor: false
showReadingTime: false
showEdit: false
---

## 2023

- **Subwoofer, B.**, Otherperson, A. (2023). The title of a fancy paper. _Journal of Important Science_, _12345_, 0–42. [https://doi.org/12.3456/78.9012345](https://doi.org/12.3456/78.9012345)

## 2022

- **Subwoofer, B.** (2022). The title of another fancy paper. _Journal of More Important Science_, _56789_, 142–242. [https://doi.org/67.8901/23.4567890](https://doi.org/67.8901/23.4567890)

- **Subwoofer, B.**, Collab, A., Otherperson, A. (2022). An interesting paper on important things. _Journal of Somewhat Important Science_, _13579_, 21–23. [https://doi.org/23.4567/89.0123456](https://doi.org/23.4567/89.0123456)

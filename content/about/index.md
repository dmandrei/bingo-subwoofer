---
title: "About Me"
date: 2020-08-14
draft: false
description: 
slug: "about"
tags: ["users", "sample"]
showDate: false
showAuthor: false
showReadingTime: false
showEdit: false
---

## Who I Am

My name's **Bingo Subwoofer** and I do interesting science on specific
things.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mi purus,
venenatis nec bibendum at, varius ut ex. Quisque cursus blandit orci nec
viverra. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam
at lacus venenatis, commodo dolor eget, semper lorem. Phasellus nec metus
sodales tortor lobortis venenatis ac ut ex.

Integer eros elit, ultricies vitae metus in, lacinia viverra magna:
- In a lorem interdum, sollicitudin lorem nec, semper ipsum.
- Aliquam egestas risus in ipsum venenatis vulputate.
- Pellentesque in lectus et leo volutpat luctus vel non leo.
- Venenatis eget urna at nisl porttitor rhoncus.

## Finite-Element Meshing

Phasellus semper ex lacinia ligula dictum dapibus. Phasellus scelerisque nec
nisi nec interdum. Nam eu pretium eros. Vestibulum ante ipsum primis in faucibus
orci luctus et ultrices posuere cubilia curae; Pellentesque in vestibulum
lectus, nec sodales neque. Nam imperdiet lectus eget elit lacinia, accumsan
dictum tortor finibus.

![](mesh.png "Nunc tortor sem, scelerisque quis metus vel, consequat suscipit tortor.")


## Wave Physics

Suspendisse potenti. Praesent sed sapien enim. Donec vehicula libero vel cursus
fringilla. Nulla in varius tellus. Nulla cursus dictum leo sit amet venenatis.
Nunc semper sagittis velit at mattis. Nullam a aliquet tortor, sit amet mollis
diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames
ac turpis egestas.

![](wavefield.png "Nulla cursus dictum leo sit amet venenatis.")

Cras mollis non tortor non malesuada. Nullam venenatis, urna eget rutrum
aliquet, turpis elit tristique sem, vel pellentesque ex nisi quis arcu. Ut ut
pretium libero. Nullam quis massa congue, rhoncus augue at, ultrices tellus.
Integer ultrices sapien a nibh tincidunt fringilla. 

## Math Explainers

Let's consider the equation

$$
    2 = 1 + 1.
$$

To make this look more impressive, let's first take

$$
    2 = \sum_{n = 0}^\infty \frac{1}{2^n}
$$

in addition to

$$
    1 = \ln{\left[ \lim_{z \to 0} \left( 1 + \frac{1}{z} \right)^z \right]}
$$

and

$$
    1 = \cosh{(y)} \sqrt{1 - \tanh^2{y}}.
$$

Finally, let's not forget about

$$
    1 = \sin^2{x} + \cos^2{x}
$$

such that we can combine everything to get

$$
    \sum_{n = 0}^\infty \frac{\sin^2{x} + \cos^2{x}}{2^n} = \ln{\left[ \lim_{z \to 0} \left( 1 + \frac{1}{z} \right)^z \right]} + \cosh{(y)} \sqrt{1 - \tanh^2{y}}.
$$

## My Other Passions

Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia
curae; Ut dignissim metus eget justo pharetra, eu mollis nisl tincidunt. In hac
habitasse platea dictumst. Morbi maximus imperdiet mi, sit amet pretium magna
blandit a. Donec dignissim tincidunt leo sit amet elementum. Donec dui ex,
consequat ac sem vitae, sollicitudin pharetra ligula.

{{< youtube dQw4w9WgXcQ >}}
